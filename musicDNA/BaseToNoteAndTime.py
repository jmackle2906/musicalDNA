import re

def readFramesToNumberArrays(nucleotideDoc):
    processed = stringPreprocessesing(nucleotideDoc)
    count = 0
    arrayOfArrays = []
    while count < 3:
        arrayOfArrays.append(stringToNumberArray(processed))
        processed = nextReadFrame(processed)
        count += 1
    return arrayOfArrays

def stringToNumberArray(nucleotideDoc):
    notes=[]
    operonit = re.compile(r"([ATGC]{3})")
    for operon in operonit.findall(nucleotideDoc):
        notes.append(operonToNumber(operon))
    return notes

def nextReadFrame(nucleotideDoc):
    operonit = re.compile(r"([ATGC]{3})")
    firstLetter = re.search(operonit, nucleotideDoc).start()
    nucleotideDoc = nucleotideDoc[0:firstLetter] + nucleotideDoc[firstLetter + 1::]
    return nucleotideDoc

def operonToNumber(operon):
    operonAsNumber = []
    for nucleotide in operon:
        if nucleotide == "A":
            operonAsNumber.append(1)
        if nucleotide == "T":
            operonAsNumber.append(2)
        if nucleotide == "G":
            operonAsNumber.append(3)
        if nucleotide == "C":
            operonAsNumber.append(4)
    return -3 + (operonAsNumber[0] * operonAsNumber[1] + operonAsNumber[2] * 2)

def stringPreprocessesing(nucleotideDoc):
    return nucleotideDoc.replace(" ","")

def createArrayOfMIDINotes(whichRange):
    if whichRange<0 or whichRange>2:
        print("error the ranges should be between 0 and 2, 0 = bass , 1= mid, 2= treble")
        return
    degrees=[]
    bassRange=[36,60]
    midRange=[48,72]
    trebleRange=[60,84]
    ranges=[bassRange, midRange, trebleRange]
    counters = ranges[whichRange]
    while counters[0] <counters[1]:
        degrees.append(counters[0])
        counters[0] += 1
    return degrees

def convertReadFramesToUsableMIDINumbers(nucleotideDoc):
    count = 0
    toBeReturned =readFramesToNumberArrays(nucleotideDoc)
    for readFrames in toBeReturned:
        for note in readFrames:
            range = createArrayOfMIDINotes(count)
            readFrames[readFrames.index(note)] = range[0]+note
        toBeReturned[toBeReturned.index((readFrames))] = readFrames
        count += 1
    return toBeReturned

def noteLengthArray(nucleotideDoc):
    notes=[]
    operonit = re.compile(r"([ATGC]{3})")
    for operon in operonit.findall(nucleotideDoc):
        notes.append(operonToTimeSignature(operon))
    return notes

def operonToTimeSignature(operon):
    nucleotide= operon[2]
    if nucleotide == "A":
        return 1
    if nucleotide == "T":
        return 2
    if nucleotide == "G":
        return 3
    if nucleotide == "C":
        return 4

def noteLengthReadFrames(nucleotideDoc):
    processed = stringPreprocessesing(nucleotideDoc)
    count = 0
    arrayOfArrays = []
    while count < 3:
        arrayOfArrays.append(noteLengthArray(processed))
        processed = nextReadFrame(processed)
        count += 1
    return arrayOfArrays

def operonToVolume(operon):
    nucleotide = operon[1]
    if nucleotide == "A":
        return -3
    if nucleotide == "T":
        return -10
    if nucleotide == "G":
        return 3
    if nucleotide == "C":
        return 10

def noteVolumeArray(nucleotideDoc):
    notes=[]
    operonit = re.compile(r"([ATGC]{3})")
    for operon in operonit.findall(nucleotideDoc):
        notes.append(operonToVolume(operon))
    return notes

def noteVolumeReadFrames(nucleotideDoc):
    processed = stringPreprocessesing(nucleotideDoc)
    count = 0
    arrayOfArrays = []
    while count < 3:
        arrayOfArrays.append(noteLengthArray(processed))
        processed = nextReadFrame(processed)
        count += 1
    return arrayOfArrays
