
import midiutil
from musicDNA import BaseToNoteAndTime
with open ("example.midi",'wb') as output_file:
    whichArray=0
    exampleString="ATCGTCGTAGTCGTAGCTGATCGTG"
    pitchFrames = BaseToNoteAndTime.convertReadFramesToUsableMIDINumbers(exampleString)
    volumeFrames = BaseToNoteAndTime.noteVolumeReadFrames(exampleString)
    durationFrames = BaseToNoteAndTime.noteLengthReadFrames(exampleString)
    myFile = midiutil.MIDIFile(numTracks=3)
    time=0
    for pitcharray in pitchFrames:

        whichElement = 0
        volumeArray = volumeFrames[whichArray]
        durationArray = durationFrames[whichArray]

        for pitch in pitcharray:
            volume=volumeArray[whichElement]
            duration=durationArray[whichElement]

            myFile.addNote(whichArray, channel=1, time=time, pitch=pitch, duration=duration, volume=100+volume)
            time=time+duration
            whichElement += 1
        whichArray += 1
    myFile.writeFile(output_file)

